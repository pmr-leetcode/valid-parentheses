package com.pajato.leetcode

import org.junit.Test
import kotlin.test.assertFalse

class ValidParenthesesTest{

    @Test // Example 1
    fun `when input is matched pair verify true`() {
        assert(parse("()"))
    }

    @Test // Example 0
    fun `when input is empty verify true`(){
        assert(parse(""))
    }

    @Test // Example 3
    fun `when input is an umatched pair verify false`(){
        assertFalse(parse("(]"))
    }

    @Test // Example 2
    fun `when input has multiple matched pairs verify true`(){
        assert(parse("()[]{}"))
    }

    @Test // Example 4
    fun `when input has multiple unmatched pairs verify false`() {
        assertFalse(parse("{(})"))
    }

    @Test // Example 5
    fun `when input has matched embedded pairs verify true`() {
        assert(parse("{()}"))
    }

}