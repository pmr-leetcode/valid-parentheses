package com.pajato.leetcode

import java.util.Stack

fun parse(input: String): Boolean {
    val stack: Stack<Char> = Stack()
    fun execute(): Boolean {
        fun isOpener(char: Char) = char == '(' || char == '{' || char == '['
        fun isCloser(char: Char) = char == ')' || char == '}' || char == ']'
        fun isClosingMatch(closer: Char, opener: Char): Boolean =
            opener == '(' && closer == ')' || opener == '{' && closer == '}' || opener == '[' && closer == ']'

        input.forEach {
            when {
                isOpener(it) -> stack.push(it)
                isCloser(it) && isClosingMatch(it, stack.peek()) -> stack.pop()
                else -> return false
            }
        }
        return stack.isEmpty()
    }

    return if (input == "") true else execute()
}