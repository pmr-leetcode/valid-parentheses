# valid-parentheses

Leetcode easy problem.  BSC - 09Mar2020

This provides a Kotlin (FP style) solution to the "valid parentheses" problem.
